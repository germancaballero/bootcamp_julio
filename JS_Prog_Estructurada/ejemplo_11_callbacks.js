// Funciones callbacks de retrollamada
function siIrAlCineGerman(capital, puntuacion, titulo, acompanantes) {
    return capital > 10 && puntuacion > 4 && titulo.length < 20 && acompanantes > 0;
}
var siIrAlCineAriadna = function (dinero, puntuacion, titulo, acompañante) {
    let bool;
    if ((dinero < 10) || (puntuacion < 5) || (titulo.length > 50) || (acompañante === 0)) {
        bool = false; 
        console.log("No puedes ir al cine");
    } else {
        bool = true; 
        console.log("Puedes ir al cine");
    }
    return bool;
}
var siIrAlCineChristian = (cap, punt, tit, acomp)  => {
    return (cap > 8 && punt > 7 && tit.length < 40 && acomp > 0);
};

// Función que emula una funciónalidad externa, un evento de usuario, el evento de carga de página
// O el evento de que se reciben datos de una llamada HTTP (fetch)
function eventoPeliculaPublicada(puntuacion, titulo, persona, funcionCallbackSiIrAlCine) {
    console.log("Ocurren cosas que tardan, se llama a un servidor o esperamos a un envento");
    // Cuando llega el momento:
        console.log(persona.capital);
        console.log(persona.acompanantes);
    let resultado = funcionCallbackSiIrAlCine(persona.capital, puntuacion, titulo, persona.acompanantes);
    if (resultado == true)
        console.log(persona.nombre + " sí va a ir a ver " + titulo);
    else
        console.log(`${persona.nombre} NO va a ir a ver ${titulo}`);
}
var german = {nombre: "German", capital: 20, acompanantes: 0};
var ariadna = {nombre: "Ariadna", capital: 15, acompanantes: 2};
var christian = {nombre: "Christian", capital: 25, acompanantes: 2};

eventoPeliculaPublicada(8, "Iron man", german, siIrAlCineGerman);
eventoPeliculaPublicada(8, "Iron man", ariadna, siIrAlCineAriadna);
eventoPeliculaPublicada(8, "Iron man", christian, siIrAlCineChristian);