// Formatos de funciones:
// Forma tradicional:   La función puede usarse incluso antes de declararse
function funcionSumar(x, y) {
    return x + y;
}
// Forma de variable:   La única diferencia es que de esta forma, 1º tiene que declararse y después ya se puede usar
var funcionSumarVar = function (x, y) { 
    return x + y;
}
// funcionSumarVar = function(x, y) { return x - y; }

// Forma de variable y objeto: 
var operacion = " + ";
var funcionSumarObj = new Function("x", "y", "{ let resultado = x " + operacion + " y; return resultado; }");

// Forma de Lambda 1: Función con  (p1, p2) => {... } Función flecha o lamda
var funcionSumarLambda = (x, y) => {  
    return x + y;
}
// Forma de Lambda 2: Función con  (p1, p2) => {... } Función flecha o lamda
// Si la usamos sin llaves, sólo podemos poner una única línea,
// que implícitamente es lo que se devuelve en return ____;
var funcionSumarLambdaReducida = (x, y) => x + y;

console.log("Prueba Forma tradicional: " + funcionSumar(2, 3)); // No hay diferencia a la hora de usarse
console.log("Prueba Forma variable: " + funcionSumarVar(3, 5)); // No sabemos que las funciones son funciones
console.log("Prueba Forma objeto: " + funcionSumarObj(3, 4));   // Hasta que las usamos y funcionan
console.log("Prueba Forma objeto: " + funcionSumarLambda(2, 1));   
console.log("Prueba Forma objeto: " + funcionSumarLambdaReducida(2, 2));  
