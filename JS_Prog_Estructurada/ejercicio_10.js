/* Función que merece la pena ir al cine:
Vas a ir al cine siempre y cuando:
 - Tengas dinero suficiente
 - La puntuación de la peli se superior a un rango (p. ej. 5)
 - Si el título no es muy largo ( < 20 caracteres)
 - Si hay acompañante. */

function siIrAlCine(capital, puntuacion, titulo, acompanantes) {
    return capital > 10 && puntuacion > 5 && titulo.length < 20 && acompanantes > 0;
}
if (siIrAlCine(15, 6, "Star  war", 1)) {
  console.log("Vamos al cine");
}
else {
  console.log("No vamos al cine");
}
var info = {
	capital: 15,
	puntuacion: 2,
	titulo: "El ascenso de Skylwaker",
	acompanantes: 5
}
var resultado = siIrAlCine(info.capital, info.puntuacion, info.titulo, info.acompanantes);   // ....
console.log(resultado ? "Vamos al cine" : "No vamos al cine");

// Formato lambda:
siIrAlCineLambda = (c, p, t, a) => c > 10 && p > 5 && t.length < 20 && a > 0;

resultado = siIrAlCineLambda(info.capital, info.puntuacion, info.titulo, info.acompanantes);   // ....
console.log(resultado ? "Vamos al cine" : "No vamos al cine");